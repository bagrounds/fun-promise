/**
 *
 * @module fun-promise
 */ /* global Promise *//* eslint-disable max-params */
;(() => {
  'use strict'

  const { inputs } = require('guarded')
  const { fold, map, reverse } = require('fun-array')
  const { ap, toPairs, ofPairs, map: omap } = require('fun-object')
  const { num, any, array, arrayOf, fun, object, tuple } = require('fun-type')
  const curry = require('fun-curry')
  const { equalDeep: equal } = require('fun-predicate')

  /**
   *
   * of :: a -> Promise a
   *
   * @function module:fun-promise.of
   *
   * @param {*} a - anything
   *
   * @return {Promise} Promise a
   */
  const of = a => Promise.resolve(a)

  /**
   *
   * arr :: (a -> b) -> a -> Promise b
   *
   * @function module:fun-promise.arr
   *
   * @param {Function} f - a -> b
   * @param {*} a - arg for f
   *
   * @return {Promise} Promise b
   */
  const arr = (f, a) => Promise.resolve(f(a))

  /**
   *
   * >>> :: (a -> Promise b) -> (b -> Promise c) -> a -> Promise c
   *
   * @function module:fun-promise.pipe
   *
   * @param {Function} f - a -> Promise b
   * @param {Function} g - b -> Promise c
   * @param {*} a - arg for f
   *
   * @return {Promise} Promise c
   */
  const pipe = (f, g, a) => f(a).then(g)

  /**
   *
   * pipeAll :: [a -> Promise b, ..., y -> Promise z] -> a -> Promise z
   *
   * @function module:fun-promise.pipeAll
   *
   * @param {Function} fs - [a -> Promise b]
   * @param {*} a - arg for f
   *
   * @return {Promise} Promise z
   */
  const pipeAll = (fs, a) => fold(curry(pipe), of, fs)(a)

  /**
   *
   * <<< :: (b -> Promise c) -> (a -> Promise b) -> a -> Promise c
   *
   * @function module:fun-promise.compose
   *
   * @param {Function} f - b -> Promise c
   * @param {Function} g - a -> Promise b
   * @param {*} a - arg for f
   *
   * @return {Promise} Promise c
   */
  const compose = (f, g, a) => g(a).then(f)

  /**
   *
   * composeAll :: [y -> Promise z, ..., a -> Promise b] -> a -> Promise z
   *
   * @function module:fun-promise.composeAll
   *
   * @param {Function} fs - [y -> Promise z]
   * @param {*} a - arg for f
   *
   * @return {Promise} Promise z
   */
  const composeAll = (fs, a) => pipeAll(reverse(fs), a)

  /**
   *
   * first :: (a -> Promise b) -> [a, c] -> Promise [b, c]
   *
   * @function module:fun-promise.first
   *
   * @param {Function} f - a -> Promise b
   * @param {Array} pair - [a, c]
   *
   * @return {Promise} Promise [b, c]
   */
  const first = (f, [a, c]) => f(a).then(b => [b, c])

  /**
   *
   * second :: (a -> Promise b) -> [c, a] -> Promise [c, b]
   *
   * @function module:fun-promise.second
   *
   * @param {Function} f - a -> Promise b
   * @param {Array} pair - [c, a]
   *
   * @return {Promise} Promise [c, b]
   */
  const second = (f, [c, a]) => f(a).then(b => [c, b])

  /**
   *
   * *** :: (a -> Promise b) -> (c -> Promise d) -> [a, c] -> Promise [b, d]
   *
   * @function module:fun-promise.both
   *
   * @param {Function} f - a -> Promise b
   * @param {Function} g - c -> Promise d
   * @param {Array} pair - [a, c]
   *
   * @return {Promise} Promise [b, d]
   */
  const both = (f, g, [a, c]) => Promise.all([f(a), g(c)])

  /**
   *
   * &&& :: (a -> Promise b) -> (a -> Promise c) -> a -> Promise [b, c]
   *
   * @function module:fun-promise.fanout
   *
   * @param {Function} f - a -> Promise b
   * @param {Function} g - a -> Promise c
   * @param {*} a - arg for f and g
   *
   * @return {Promise} Promise [b, c]
   */
  const fanout = (f, g, a) => Promise.all([f(a), g(a)])

  /**
   *
   * left :: ((l | r) -> Boolean) ->
   *   (l -> Promise y) -> (l | r) -> Promise (y | r)
   *
   * @function module:fun-promise.left
   *
   * @param {Function} p - (l | r) -> Boolean
   * @param {Function} l2y - l -> Promise y
   * @param {*} lr - (l | r)
   *
   * @return {Promise} Promise (y | r)
   */
  const left = (p, l2y, lr) => p(lr) ? of(lr) : l2y(lr)

  /**
   *
   * right :: ((l | r) -> Boolean) ->
   *   (r -> Promise y) -> (l | r) -> Promise (l | y)
   *
   * @function module:fun-promise.right
   *
   * @param {Function} p - (l | r) -> Boolean
   * @param {Function} r2y - r -> Promise y
   * @param {*} lr - (l | r)
   *
   * @return {Promise} Promise (l | y)
   */
  const right = (p, r2y, lr) => p(lr) ? r2y(lr) : of(lr)

  /**
   *
   * +++ :: ((l | r) -> Boolean) ->
   *   (l -> Promise y) -> (r -> Promise z) -> (l | r) -> Promise (y | z)
   *
   * @function module:fun-promise.either
   *
   * @param {Function} p - (l | r) -> Boolean
   * @param {Function} l2y - l -> Promise y
   * @param {Function} r2z - r -> Promise z
   * @param {*} lr - (l | r)
   *
   * @return {Promise} Promise (y | z)
   */
  const either = (p, l2y, r2z, lr) => p(lr) ? r2z(lr) : l2y(lr)

  /**
   *
   * unfold :: (a -> Promise a) -> (a -> Boolean) -> a -> Promise a
   *
   * @function module:fun-promise.unfold
   *
   * @param {Function} next - a -> Promise a
   * @param {Function} stop - a -> Boolean
   * @param {*} value - to start with
   *
   * @return {Promise} Promise a
   */
  const unfold = (next, stop, value) =>
    stop(value) ? of(value) : next(value).then(v => unfold(next, stop, v))

  /**
   * traverseArray :: (a -> Promise b) -> [a] -> Promise [b]
   *
   * @param {Function} f - a -> Promise b
   * @param {Function} t - [a]
   *
   * @return {Promise} Promise [b]
   */
  const traverseArray = (f, t) => Promise.all(map(f, t))

  /**
   * traverseObject :: (a -> Promise b) -> {a} -> Promise {b}
   *
   * @param {Function} f - a -> Promise b
   * @param {Function} t - {a}
   *
   * @return {Promise} Promise {b}
   */
  const traverseObject = (f, t) =>
    Promise.all(map(([k, a]) => f(a).then(b => [k, b]), toPairs(t)))
      .then(ofPairs)

  /**
   *
   * lmap :: (a -> b) -> (b -> Promise c) -> a -> Promise c
   *
   * @function module:fun-promise.lmap
   *
   * @param {Function} f - a -> b
   * @param {Function} g - b -> Promise c
   * @param {*} a - to feed to f
   *
   * @return {Promise} Promise c
   */
  const lmap = (f, g, a) => g(f(a))

  /**
   *
   * rmap :: (b -> c) -> (a -> Promise b) -> a -> Promise c
   *
   * @function module:fun-promise.rmap
   *
   * @param {Function} f - b -> c
   * @param {Function} g - a -> Promise b
   * @param {*} a - to feed to g
   *
   * @return {Promise} Promise c
   */
  const rmap = (f, g, a) => g(a).then(f)

  /**
   *
   * dimap :: (a -> b) -> (c -> d) -> (b -> Promise c) -> a -> Promise d
   *
   * @function module:fun-promise.dimap
   *
   * @param {Function} f - a -> b
   * @param {Function} g - c -> d
   * @param {Function} h - b -> Promise c
   * @param {*} a - to feed to f
   *
   * @return {Promise} Promise c
   */
  const dimap = (f, g, h, a) => h(f(a)).then(g)

  /**
   *
   * timeout :: number -> (a -> Promise b) -> a -> Promise b
   *
   * @function module:fun-promise.timeout
   *
   * @param {Number} n - timeout
   * @param {Function} f - a -> Promise b
   * @param {*} a - argument to f
   *
   * @return {Promise} Promise b
   */
  const timeout = (n, f, a) => Promise.race([
    new Promise((resolve, reject) =>
      setTimeout(() => reject(Error(`Timeout after ${n}ms`)), n)),
    f(a)
  ])

  /**
   *
   * equalFor :: a -> (a -> Promise b) -> (a -> Promise b) -> Promise bool
   *
   * @function module:fun-promise.equalFor
   *
   * @param {*} a - argument to f and g
   * @param {Function} f - a -> Promise b
   * @param {Function} g - a -> Promise b
   *
   * @return {Promise} Promise bool // if f(a) === g(a)
   */
  const equalFor = (a, f, g) => Promise.all([
    f(a).then(r => ['result', r]).catch(e => ['error', e]),
    g(a).then(r => ['result', r]).catch(e => ['error', e])
  ]).then(([fx, gx]) => equal(fx, gx))

  /**
   *
   * reject :: a -> Promise.rejected a
   *
   * @function module:fun-promise.reject
   *
   * @param {*} a - value to reject with
   *
   * @return {Promise} Promise.rejected a
   */
  const reject = a => Promise.reject(a)

  /**
   *
   * delay :: number -> a -> Promise a
   *
   * @function module:fun-promise.delay
   *
   * @param {Number} n - milliseconds to sleep for
   * @param {*} a - value to return
   *
   * @return {Promise} Promise a
   */
  const delay = (n, a) =>
    new Promise((resolve) => setTimeout(() => resolve(a), n))

  /**
   *
   * retry :: (Number -> Number) -> ((Number, Error) -> Boolean) -> number ->
   *   (a -> Promise b) -> a -> Promise b
   *
   * @function module:fun-promise.retry
   *
   * @param {Function} next - Number -> Number
   * @param {Function} stop - (Number, Error) -> Boolean
   * @param {Number} d0 - initial duration to wait before retrying
   * @param {Function} f - a -> Promise b
   * @param {*} a - to feed to f
   *
   * @return {Promise} Promise b
   */
  const retry = (next, stop, d0, f, a) => f(a)
    .catch(e => stop(d0, e)
      ? Promise.reject(e)
      : delay(d0, next(d0))
          .then(d1 => retry(next, stop, d1, f, a)))

  const api = { of, arr, pipe, first, second, both, fanout, left, right,
    either, unfold, traverseArray, traverseObject, compose, pipeAll,
    composeAll, lmap, rmap, dimap, retry, delay, timeout, equalFor, reject }

  const guards = omap(inputs, {
    of: tuple([any]),
    arr: tuple([fun, any]),
    pipe: tuple([fun, fun, any]),
    pipeAll: tuple([arrayOf(fun), any]),
    compose: tuple([fun, fun, any]),
    composeAll: tuple([arrayOf(fun), any]),
    first: tuple([fun, tuple([any, any])]),
    second: tuple([fun, tuple([any, any])]),
    both: tuple([fun, fun, tuple([any, any])]),
    fanout: tuple([fun, fun, any]),
    left: tuple([fun, fun, any]),
    right: tuple([fun, fun, any]),
    either: tuple([fun, fun, fun, any]),
    unfold: tuple([fun, fun, any]),
    traverseArray: tuple([fun, array]),
    traverseObject: tuple([fun, object]),
    lmap: tuple([fun, fun, any]),
    rmap: tuple([fun, fun, any]),
    dimap: tuple([fun, fun, fun, any]),
    retry: tuple([fun, fun, num, fun, any]),
    delay: tuple([num, any]),
    timeout: tuple([num, fun, any]),
    equalFor: tuple([any, fun, fun]),
    reject: tuple([any])
  })

  module.exports = omap(curry, ap(guards, api))
})()

