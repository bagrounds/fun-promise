# [fun-promise](https://bagrounds.gitlab.io/fun-promise)

Promise Arrow Combinators

[![build-status](https://gitlab.com/bagrounds/fun-promise/badges/master/build.svg)](https://gitlab.com/bagrounds/fun-promise/commits/master)
[![coverage-report](https://gitlab.com/bagrounds/fun-promise/badges/master/coverage.svg)](https://gitlab.com/bagrounds/fun-promise/commits/master)
[![license](https://img.shields.io/npm/l/fun-promise.svg)](https://www.npmjs.com/package/fun-promise)
[![version](https://img.shields.io/npm/v/fun-promise.svg)](https://www.npmjs.com/package/fun-promise)
[![downloads](https://img.shields.io/npm/dt/fun-promise.svg)](https://www.npmjs.com/package/fun-promise)
[![downloads-monthly](https://img.shields.io/npm/dm/fun-promise.svg)](https://www.npmjs.com/package/fun-promise)
[![dependencies](https://david-dm.org/bagrounds/fun-promise/status.svg)](https://david-dm.org/bagrounds/fun-promise)

## [Test Coverage](https://bagrounds.gitlab.io/fun-promise/coverage/lcov-report/index.html)

## [API Docs](https://bagrounds.gitlab.io/fun-promise/index.html)

## Dependencies

### Without Tests

![Dependencies](https://bagrounds.gitlab.io/fun-promise/img/dependencies.svg)

### With Tests

![Test Dependencies](https://bagrounds.gitlab.io/fun-promise/img/dependencies-test.svg)

