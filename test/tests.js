/* global Promise */
;(() => {
  'use strict'

  /* imports */
  const { concat, map } = require('fun-array')
  const { equalDeep: equal, not } = require('fun-predicate')
  const { map: oMap } = require('fun-object')
  const { curry } = require('fun-function')
  const { gt, mul, add } = require('fun-scalar')

  const even = x => (x % 2) === 0

  const { arrOverPipe, firstOverPipe, arrFirst, firstPipeArrFst,
    arrOf, firstOfBoth, firstSecondBoth, fanoutDupBoth, leftArr,
    leftOverPipe, leftNotRight, leftEitherOf, pipeCompose,
    dimapOfOf, dimapLmapRmap, delayOf, retryOf, timeoutFail,
    timeoutPass } = (() => {
    /* eslint-disable max-params */
      const fst = ([a, b]) => a
      const dup = x => [x, x]
      const i = x => x
      const uncurry = f => ([a, b]) => f(a, b)
      const pEq = (eq, a, b) => Promise.all([a, b]).then(uncurry(eq))

      return oMap(curry, {

        /* Arrow Laws */
        pipeCompose: ([eq, f, g, xs], { pipe, compose }) =>
          pEq(eq, pipe(f, g)(xs), compose(g, f)(xs)),
        arrOverPipe: ([eq, f, g, xs], { arr, pipe }) =>
          pEq(eq, arr(pipe(f, g))(xs), pipe(arr(f), arr(g))(xs)),
        firstOverPipe: ([eq, f, g, xs], { first, pipe }) =>
          pEq(eq, first(pipe(f, g))(xs), pipe(first(f), first(g))(xs)),
        arrFirst: ([eq, f, xs], { first, arr }) =>
          pEq(eq, first(arr(f))(xs), arr(first(f))(xs)),
        firstPipeArrFst: ([eq, f, xs], { pipe, first, arr }) =>
          pEq(eq, pipe(first(f), arr(fst))(xs), pipe(arr(fst), f)(xs)),
        arrOf: ([eq, xs], { arr, of }) =>
          pEq(eq, arr(i)(xs), of(xs)),
        firstOfBoth: ([eq, f, g, xs], { arr, of, first, pipe, both }) =>
          pEq(eq,
            pipe(first(f), arr(both(of, g)))(xs),
            pipe(arr(both(of, g)), first(f))(xs)
          ),
        firstSecondBoth: ([eq, f, g, xs], { pipe, first, second, both }) =>
          pEq(eq, pipe(first(f), second(g))(xs), both(f, g)(xs)),
        fanoutDupBoth: ([eq, f, g, xs], { arr, fanout, pipe, both }) =>
          pEq(eq, fanout(f, g)(xs), pipe(arr(dup), both(f, g))(xs)),

        /* ArrowChoice Laws */
        leftArr: ([eq, p, f, xs], { arr, left }) =>
          pEq(eq, left(p, arr(f))(xs), arr(left(p, f))(xs)),
        leftNotRight: ([eq, p, f, xs], { arr, left, right }) =>
          pEq(eq, left(p, f)(xs), right(not(p), f)(xs)),
        leftEitherOf: ([eq, p, f, xs], { of, left, either }) =>
          pEq(eq, left(p, f)(xs), either(p, f, of)(xs)),
        leftOverPipe: ([eq, p, f, g, xs], { pipe, left }) =>
          pEq(eq, left(p, pipe(f, g))(xs), pipe(left(p, f), left(p, g))(xs)),

        /* Profunctor Laws */
        dimapOfOf: ([eq, f, xs], { dimap }) =>
          pEq(eq, dimap(i, i, f)(xs), f(xs)),
        dimapLmapRmap: ([eq, f, g, h, xs], { dimap, lmap, rmap }) =>
          pEq(eq, dimap(g, h, f)(xs), lmap(g, rmap(h, f))(xs)),

        delayOf: ([eq, xs], { delay, of }) => pEq(eq, of(xs), delay(0)(xs)),
        retryOf: ([eq, xs], { retry, of }) =>
          pEq(eq, of(xs), retry(x => x, x => false, 0, of)(xs)),
        timeoutFail: ([x], { timeout, equalFor, reject, delay }) =>
          equalFor(x, reject, timeout(1, delay(10))),
        timeoutPass: ([x], { timeout, equalFor, reject, delay, of }) =>
          equalFor(x, of, timeout(10, delay(1)))
      })
    /* eslint-enable max-params */
    })()

  const testP = p => (s, callback) => p(s)
    .then(r => callback(null, r))
    .catch(e => callback(e))

  const s = { arr: f => x => Promise.resolve(f(x)) }

  const xs = [7, 9]
  const propertyTests = [
    arrFirst([equal, s.arr(add(3)), xs]),
    firstOverPipe([equal, s.arr(add(3)), s.arr(mul(5)), xs]),
    firstPipeArrFst([equal, s.arr(add(3)), xs]),
    arrOf([equal, xs]),
    pipeCompose([equal, s.arr(add(3)), s.arr(mul(5)), 3]),
    arrOverPipe([equal, s.arr(add(3)), s.arr(mul(5)), 3]),
    firstOfBoth([equal, s.arr(add(3)), s.arr(mul(5)), xs]),
    firstSecondBoth([equal, s.arr(add(3)), s.arr(mul(5)), xs]),
    fanoutDupBoth([equal, s.arr(add(3)), s.arr(mul(5)), 7]),
    leftArr([equal, even, s.arr(mul(5)), 7]),
    leftArr([equal, even, s.arr(mul(5)), 8]),
    leftOverPipe([equal, even, s.arr(add(3)), s.arr(mul(5)), 8]),
    leftNotRight([equal, even, s.arr(add(3)), 8]),
    leftNotRight([equal, even, s.arr(add(3)), 7]),
    leftEitherOf([equal, even, s.arr(add(3)), 7]),
    leftEitherOf([equal, even, s.arr(add(3)), 8]),
    dimapOfOf([equal, s.arr(add(3)), 8]),
    dimapLmapRmap([equal, s.arr(add(3)), mul(5), add(7), 8]),
    delayOf([equal, 3]),
    retryOf([equal, 3]),
    timeoutPass([3]),
    timeoutFail([Error('message')])
  ]

  const equalityTests = [
    ({ arr, unfold }) => unfold(arr(x => x * 2), gt(100), 1)
      .then(equal(128)),
    ({ arr, traverseArray }) => traverseArray(arr(x => x * 2), [1, 2, 3])
      .then(equal([2, 4, 6])),
    ({ arr, traverseObject }) =>
      traverseObject(arr(x => x * 2), { a: 1, b: 2, c: 3 })
        .then(equal({ a: 2, b: 4, c: 6 })),
    ({ arr, pipeAll }) => pipeAll([arr(mul(2)), arr(add(3)), arr(mul(-3))], 7)
        .then(equal(-51)),
    ({ arr, pipeAll }) => pipeAll([arr(mul(2)), arr(add(3))], 7)
        .then(equal(17)),
    ({ arr, pipeAll }) => pipeAll([arr(mul(2))], 7).then(equal(14)),
    ({ arr, pipeAll }) => pipeAll([], 7).then(equal(7)),
    ({ arr, composeAll }) =>
      composeAll([arr(mul(2)), arr(add(3)), arr(mul(-3))], 7)
        .then(equal(-36)),
    ({ arr, composeAll }) => composeAll([arr(mul(2)), arr(add(3))], 7)
        .then(equal(20)),
    ({ arr, composeAll }) => composeAll([arr(mul(2))], 7).then(equal(14)),
    ({ arr, composeAll }) => composeAll([], 7).then(equal(7))
  ]

  /* exports */
  module.exports = map(testP, concat(propertyTests, equalityTests))
})()

